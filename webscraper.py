import requests
import bs4
import re

url = input("input url")
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0'}
sub_url = re.sub("https://ncode\.syosetu\.com","",url)

page = requests.get(url, headers=headers)

print(page)

#User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0

soup = bs4.BeautifulSoup(page.content, 'html.parser')

links = soup.find_all('a')

novel_link = []

for link in links:
    re_match = re.match(sub_url + '.+', link['href'])

    if re_match is not None:
        novel_link.append(link['href'])

print(novel_link)

processed_novel_links = [re.sub(sub_url, "", chapter) for chapter in novel_link]

for chapter in processed_novel_links:
    page = requests.get(url + chapter, headers=headers)
    f = open('html/' + re.sub(r"[\\:/.]", "", url + chapter + 'index.html') + '.html', 'w', newline='', encoding='utf8')
    try:
        f.write(page.text)
    except:
        print("############ FAILED ###############")
    print(chapter)
    print('html/' + re.sub(r"[\\:/.]", "", url + chapter + 'index.html') + '.html')
    f.close()

