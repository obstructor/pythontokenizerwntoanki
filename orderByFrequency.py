import csv

f = open("final_file.tsv", 'r', encoding='utf8', newline='')
o = open("ordered_file.csv", 'w', encoding='utf8', newline='')

ncodes = {}
with open("ncodes.txt", 'r', encoding='utf8', newline='') as file:
    for line in file:
        ncode = str(line).split(' ')
        ncodes[ncode[0]] = ncode[1].strip('\r\n \t')

headers=[]
with open("header.csv", 'r', encoding='utf8', newline='') as file:
    for line in file:
        head = str(line).split('\t')
        for i in range(len(head)):
            column = head[i]
            if '_freq' in column:
                headers.append((i, ncodes[column.split('_')[0]]))

reader = csv.reader(f, delimiter='\t')
writer = csv.writer(o, delimiter='\t')

data = []

def sorter_func(x):
    return int(x[6])


def gen_tags(row):
    global headers
    tags = []
    for h in headers:
        freq = row[h[0]]
        if freq is None or freq=="":
            continue
        elif int(freq) <= 3:
            tags.append(f"low{h[1]}Freq")
    return ' '.join(tags)

for row in reader:
    data.append(row)

data.sort(reverse=True,key=sorter_func)

i = 0
for row in data:
    print(row[6])
    row[-1] += gen_tags(row)
    row[0] = row[1]
    row[1] = i
    i = i + 1
    if int(row[6]) > 3:
        writer.writerow(row)

f.close()
o.close()
