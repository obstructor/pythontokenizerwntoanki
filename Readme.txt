Dictionaries, download the yomichan versions.  
This project parses the term_bank.json files.  
I personally use 大辞林第三版 (diajiriin)
https://anacreondjt.gitlab.io/docs/dicts/

Put JP dictionary into:
daijiriin/

Put EN dictionary into:
jmdict_english/


Run webscraper.py to download web novels from ncode.syosetu

Run main.py to run the flashcard builder program.  
It will read all the html files downloaded by the webscraper, 
build the dictionaries, and create an output tsv file.

Run orderByFrequency.py to order the deck and create the "lowFreq" tags.  
orderByFrequency requires ncodes.txt to be modified with the ncodes you are using to populate the tags.

If you want to exclude certain tags from the core decks.  
Put core30kwithTags.csv into Core10k/.
Then update the "ignore_tags" list at the top of main.py


Create anki template for the cards as desired.  I use a simple:

Front:
	{{Word}}
	<div style="text-align:left">{{Sentence}}</div>

Back:
	{{FrontSide}}

	<hr id=answer>

	{{Reading}}
	<div style="text-align:left">{{Japanese_Def}}</div>
	<div style="text-align:left">{{English_Def}}</div>

Styling:
	.card {
	 font-family: arial;
	 font-size: 20px;
	 text-align: center;
	 color: black;
	 background-color: white;
	}

	b{color: blue;
	 font-size: 22px;
	 font-family: arial-bold;}
