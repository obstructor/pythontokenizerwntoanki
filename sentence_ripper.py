from bs4 import BeautifulSoup
import re
import os

replace_regex = "[a-zA-Z0-9<>_\\- \\t\\n/　]"
tokenizer_replace_regex = "[a-zA-Z0-9<>_\\-,\\. \\t\\n/　？「」！『』：…《》＊（）／［］・()｛｝＝【】≪≫｜゛〈〉〔〕〇○+=←☆，。´∀｀*“”、「?]"
total_count = 0
unique_chars = {}
sentences = []
unique_books = []


class Sentence:
    def __init__(self, line, book, chapter):
        self.line = line
        self.book = book
        self.chapter = chapter


def Print_Novel_Text(file_name):
    global total_count, unique_chars
    file = open(file_name,'r+', encoding="utf8").read()

    soup = BeautifulSoup(file, "html.parser")

    a = soup.find_all(attrs={'id': "novel_honbun"})

    for i in a:
        x = i.find_all('p')
        for child in x:
            total_count += 1
            if isinstance(child.contents[0], str):
                lines = child.contents[0].split('。')
                for line in lines:
                    clean_line = re.sub(tokenizer_replace_regex, "", line)
                    for char in clean_line:
                        unique_chars[char] = unique_chars[char] + 1 if char in unique_chars else 1


def Get_File_Sentences(file_name):
    result = []
    file = open(file_name, 'r+', encoding="utf8").read()
    print(file_name)
    match = re.match(".*httpsncodesyosetucom(n[0-9]+[a-zA-Z]+)([0-9]+)indexhtml.*", file_name)
    if match is None:
        print("FAILURE: " + file_name)
        return []
    book = match.group(1)
    if book not in unique_books:
        unique_books.append(book)
    chapter = match.group(2)
    novel_honbun = BeautifulSoup(file, "html.parser").find_all(attrs={'id': "novel_honbun"})

    for novel_honbun_divs in novel_honbun:
        paragraphs = novel_honbun_divs.find_all('p')
        for p in paragraphs:
            if isinstance(p.contents[0], str):
                lines = p.contents[0].split('。')
                for line in lines:
                    clean_line = re.sub(tokenizer_replace_regex, "", line)
                    line = re.sub(replace_regex, "", line)
                    if len(clean_line) > 4:
                        result.append(Sentence(line, book, int(chapter)))
    return result


def Get_All_Sentences():
    result = []
    for root, dir, files in os.walk(os.curdir):
        for file in files:
            if '.html' in file:
                result = result + Get_File_Sentences(os.path.join(root, file))
    return result


def Get_Example_Sentences():
    return Get_File_Sentences("html/1/index.html")


if __name__ == '__main__':
    print(Get_Example_Sentences())