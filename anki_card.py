import re

class AnkiCard():
    def __init__(self, dict_form, readings, english, japanese, line_data, score):
        self.count = 1
        self.dict_form = dict_form
        self.readings = re.sub("\\n", "<br>", "<br>".join(readings))
        self.english_def = re.sub("\\n", "<br>", "<br><br>".join(english))
        self.japanese_def = re.sub("\\n", "<br>", "<br><br>".join(japanese))
        self.sentence = line_data.line
        self.sentence_score = score

        self.first_chapter_occurrence = {line_data.book: line_data.chapter}
        self.book_frequency = {line_data.book: 1}

    def Collision(self, line_data, score):
        if score > self.sentence_score:
            self.sentence = line_data.line
            self.sentence_score = score

        if line_data.book not in self.first_chapter_occurrence:
            self.first_chapter_occurrence[line_data.book] = line_data.chapter
        else:
            self.first_chapter_occurrence[line_data.book] = min(self.first_chapter_occurrence[line_data.book], line_data.chapter)

        self.book_frequency[line_data.book] = 1 if line_data.book not in self.book_frequency else self.book_frequency[line_data.book] + 1
        self.count += 1
