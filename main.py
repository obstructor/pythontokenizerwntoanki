from sudachipy import tokenizer
from sudachipy import dictionary
import sentence_ripper
import core_reader
import jj_dict
import anki_card
import re


def get_sentence_score(sentence, tokens):
    length = len(sentence)
    if length < 25:
        sentence_score = length * 10
    else:
        sentence_score = 250 - (length - 25) * 2

    #sentence_score = 10 * len(sentence) - .15  * len(sentence) * len(sentence) # weighting to 30 character sentences
    sentence_uniqueness = []
    for token in tokens:
        part = token.part_of_speech()[0]
        if part in ['助動詞', '接続詞', '感動詞']: # empty words like しかし, interjections, helper verbs
            sentence_score -= 30 if length < 35 else 0
        elif part in ['補助記号']: # symbols
            sentence_score -= 30 if length < 30 else 10
        elif part in sentence_uniqueness and part not in ['助詞']: #particles do not contribute to uniqueness
            sentence_score -= 30 if length < 30 else 10
        sentence_uniqueness.append(part)
    return sentence_score

tokenizer_replace_regex = "[a-zA-Z0-9<>_\\-,\\. \\t\\n/　？「」！『』：…《》＊（）／［］・()｛｝＝【】≪≫｜゛〈〉〔〕〇○+=←☆，。´∀｀*“”、\\[\\]\\(\\)]"
ignore_parts_of_speech = ['補助記号','助詞','感動詞','助動詞','接続詞']
#ignore_tags = ['Core1000', 'Core2000']
ignore_tags = []
ignore_tokens = {}
coreVocab = core_reader.Get_All_Core()

for key in coreVocab:
    for ignore in ignore_tags:
        if ignore in coreVocab[key]:
            ignore_tokens[key] = True


token_count = {}
#Symbols, Particles, Interjections, Helper verbs, Conjuction

sentences = sentence_ripper.Get_All_Sentences()
#sentences = sentence_ripper.Get_Example_Sentences()

tokenizer_obj = dictionary.Dictionary().create()
mode = tokenizer.Tokenizer.SplitMode.C
jj_dictionary = jj_dict.Get_Dictionary()
anki_cards = {}
count = 0

for line_data in sentences:
    count += 1
    if count % (len(sentences) // 100) == 0:
        print(f"Percent processed {count // (len(sentences) // 100)}")

    sentence = line_data.line
    tokens = tokenizer_obj.tokenize(re.sub(tokenizer_replace_regex, "", sentence), mode)
    sentence_score = get_sentence_score(sentence, tokens)
        
    #print(sentence)
    for token in tokens:
        dict_form = token.dictionary_form()
        part = token.part_of_speech()[0]
        try:
            line_data.line = re.sub(token.surface(),"<b>" + token.surface() + "</b>", sentence)
        except:
            continue
        #ignore if bad part of speech
        #ignore if in Core
        if part in ignore_parts_of_speech or dict_form in ignore_tokens:
            continue

        if dict_form in jj_dictionary:
            if dict_form in anki_cards:
                anki_cards[dict_form].Collision(line_data, sentence_score)
            else:
                dictionary_entry = jj_dictionary[dict_form]
                anki_cards[dict_form] = anki_card.AnkiCard(dict_form, dictionary_entry.reading, dictionary_entry.english_def, dictionary_entry.japanese_def, line_data, sentence_score)
        else:
            #print(dict_form)
            pass


f = open("final_file.tsv", 'w', encoding="utf8")

i = 0
print(sentence_ripper.unique_books)

with open("header.csv", 'w', encoding='utf8') as w:
    w.write("order")
    w.write('\tkey\tcard.readings\tcard.japanese_def\tcard.english_def\tcard.sentence\tcard.count\t')
    for book in sentence_ripper.unique_books:
        w.write(book + '_freq\t' + book + '_first\t')
    w.write('\n')


for key in anki_cards:
    card = anki_cards[key]

    book_freq_data = ""
    for book in sentence_ripper.unique_books:
        book_freq_data += str(card.book_frequency[book]) if book in card.book_frequency else "0"
        book_freq_data += '\t'
        book_freq_data += str(card.first_chapter_occurrence[book]) if book in card.first_chapter_occurrence else "999999"
        book_freq_data += '\t'

    f.write(str(i) + '\t' + key + '\t' + card.readings + '\t' + card.japanese_def + '\t' + card.english_def + '\t' + card.sentence + '\t' + str(card.count) + '\t' + book_freq_data + '\n')
    i += 1

f.close()