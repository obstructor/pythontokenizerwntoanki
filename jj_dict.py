import json
import os
import re
from sudachipy import tokenizer
from sudachipy import dictionary

jj_dict = {}
has_run = False

tokenizer_obj = dictionary.Dictionary().create()
mode = tokenizer.Tokenizer.SplitMode.C

class Dict_Entry():
    def __init__(self):
        self.japanese_def = []
        self.english_def = []
        self.reading = []

    def Join_English(self, reading, e_def):
        self.english_def = list(set(self.english_def + e_def))
        self.reading = list(set(self.reading + (reading if isinstance(reading, list) else [reading])))
        pass

    def Join_Japanese(self, reading, j_def):
        self.japanese_def = list(set(self.japanese_def + j_def))
        self.reading = list(set(self.reading + (reading if isinstance(reading, list) else [reading])))
        pass


def Read_File(file_name, type):
    global jj_dict
    f = open(file_name, encoding="utf8")
    a = json.loads(f.read())

    for ob in a:
        tokens = tokenizer_obj.tokenize(ob[0],mode)
        if len(tokens) > 1:
            continue
        key = tokenizer_obj.tokenize(ob[0],mode)[0].dictionary_form()
        reading = ob[1]
        definitions = ob[5]

        if key in jj_dict:
            if type == 'JP':
                jj_dict[key].Join_Japanese(reading, definitions)
            elif type == 'EN':
                jj_dict[key].Join_English(reading, definitions)
        else:
            d_entry = Dict_Entry()
            d_entry.reading += (reading if isinstance(reading, list) else [reading])

            if type == 'JP':
                d_entry.japanese_def += definitions
            elif type == 'EN':
                d_entry.english_def += definitions

            jj_dict[key] = d_entry


def Read_JJ_Dict():
    global jj_dict
    file_list = {}
    count = 1
    for root, dir, files in os.walk(os.path.join(os.curdir, 'daijiriin')):
        for file in files:
            if 'term' in file:
                file_list[int(re.search("[0-9]+", file).group(0))] = os.path.join(root, file)
                count += 1
    for i in range(1, count):
        print(file_list[i])
        Read_File(file_list[i], "JP")


def Read_EN_Dict():
    global jj_dict
    file_list = {}
    count = 1
    for root, dir, files in os.walk(os.path.join(os.curdir, 'jmdict_english')):
        for file in files:
            if 'term' in file:
                file_list[int(re.search("[0-9]+", file).group(0))] = os.path.join(root, file)
                count += 1
    for i in range(1, count):
        print(file_list[i])
        Read_File(file_list[i], "EN")


def Get_Dictionary():
    global jj_dict, has_run
    if has_run:
        return jj_dict
    else:
        has_run = True
        Read_JJ_Dict()
        Read_EN_Dict()
        return jj_dict


if __name__ == '__main__':
    print(Get_Dictionary())
