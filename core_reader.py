import re
import csv
from sudachipy import tokenizer
from sudachipy import dictionary


tokenizer_obj = dictionary.Dictionary().create()
mode = tokenizer.Tokenizer.SplitMode.C
unique_tags = []


def Get_All_Core():
    global unique_tags
    result = {}

    f = open('Core10k/core30KwithTags.csv', newline='', encoding='utf8')
    reader = csv.reader(f)
    for line in reader:
        tags = re.sub("\\.", " ", line[2]).split(' ')

        token_morpheme_list = tokenizer_obj.tokenize(line[1], mode)
        token = token_morpheme_list[0]

        result[token.dictionary_form()] = list(set(tags + result[token.dictionary_form()])) if token.dictionary_form() in result else tags

        #print(token.dictionary_form() + '\t' + str(tags))
        unique_tags = unique_tags + tags
    return result


if __name__ == "__main__":
    print(Get_All_Core())
    print(list(set(unique_tags)))