from sudachipy import tokenizer
from sudachipy import dictionary



tokenizer_obj = dictionary.Dictionary().create()
mode = tokenizer.Tokenizer.SplitMode.C
sentence = '「「「「「「「「「「俺達は戦う！！」」」」」」」」」」'
tokens = tokenizer_obj.tokenize(sentence, mode)
# sentence_score = 10 * len(sentence) - .15  * len(sentence) * len(sentence)
# print(sentence_score)
# sentence_uniqueness = []
# for token in tokens:
#     part = token.part_of_speech()[0]
#     message = ""
#     if part in ['助動詞', '接続詞', '感動詞']: # empty words like しかし, interjections, helper verbs
#         sentence_score -= 3
#         message += " empty "
#     elif part in ['補助記号']: # symbols
#         sentence_score -= 10
#         message += " symbols "
#     elif part in sentence_uniqueness and part not in ['助詞']: #particles do not contribute to uniqueness
#         sentence_score -= 5
#         message += " uniquness "
#     sentence_uniqueness.append(part)
#     print(f"score {part}: {message}")
# print(sentence_score)


length = len(sentence)
if length < 20:
    sentence_score = length * 10
else:
    sentence_score = 200 - (length - 20) * 2

#sentence_score = 10 * len(sentence) - .15  * len(sentence) * len(sentence) # weighting to 30 character sentences
sentence_uniqueness = []
message = ""
for token in tokens:
    message = ""
    part = token.part_of_speech()[0]
    if part in ['助動詞', '接続詞', '感動詞']: # empty words like しかし, interjections, helper verbs
        sentence_score -= 30 if length < 20 else 0
        message += " empty "
    elif part in ['補助記号']: # symbols
        sentence_score -= 30 if length < 20 else 10
        message += " symbols "
    elif part in sentence_uniqueness and part not in ['助詞']: #particles do not contribute to uniqueness
        sentence_score -= 30 if length < 20 else 10
        message += " uniquness "
    sentence_uniqueness.append(part)
    print(f"score {part}: {message}")
print(sentence_score)